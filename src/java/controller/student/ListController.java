/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.student;

import controller.authentication.BaseRequiredAuthenticaticationController;
import dao.DepartmentDBContext;
import dao.StudentDBContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Department;
import model.Student;

/**
 *
 * @author sonnt
 */
public class ListController extends BaseRequiredAuthenticaticationController {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {

        StudentDBContext sdbdb = new StudentDBContext();
        ArrayList<Student> slist = sdbdb.list();
        DepartmentDBContext deptDB = new DepartmentDBContext();
        ArrayList<Department> depts = deptDB.list();
        request.setAttribute("depts", depts);
        request.setAttribute("sizeStudent", slist.size());
        request.setAttribute("listStudent", slist);
        request.getRequestDispatcher("../view/student/list.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
    }

}
