/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.student;

import controller.authentication.BaseRequiredAuthorizationController;
import dao.DepartmentDBContext;
import dao.SkillDBContext;
import dao.StudentDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import model.Account;
import model.Department;
import model.Skill;
import model.Student;

/**
 *
 * @author lyqua
 */
public class RegisterController extends BaseRequiredAuthorizationController{

    @Override
    protected void doAuthPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        Student student = new Student();
        student.setName(request.getParameter("name"));
        student.setGender(request.getParameter("gender").equals("male"));
        student.setDob(Date.valueOf(request.getParameter("dob")));
        Department d = new Department();
        d.setId(Integer.parseInt(request.getParameter("did")));
        student.setDept(d);
        String[] skids = request.getParameterValues("skid");
        for (String skid : skids) {
            Skill sk = new Skill();
            sk.setId(Integer.parseInt(skid));
            student.getSkills().add(sk);
        }
        student.setCreateby(acc);
        StudentDBContext db = new StudentDBContext();
        db.insert(student);
        response.getWriter().println(student.getId() + " was inserted successful!");
    }

    @Override
    protected void doAuthGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        DepartmentDBContext db = new DepartmentDBContext();
        ArrayList<Department> depts = db.list();
        request.setAttribute("depts", depts);
        
        SkillDBContext skDB = new SkillDBContext();
        ArrayList<Skill> skills = skDB.list();
        request.setAttribute("skills", skills);
        request.getRequestDispatcher("../view/student/register.jsp").forward(request, response);
    }
    
}
