/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.student;

import controller.authentication.BaseRequiredAuthenticaticationController;
import controller.authentication.BaseRequiredAuthorizationController;
import dao.DepartmentDBContext;
import dao.StudentDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.ArrayList;
import model.Account;
import model.Department;
import model.Student;

/**
 *
 * @author sonnt
 */
public class CreateController extends BaseRequiredAuthorizationController {

    @Override
    protected void doAuthPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        //validate data
        Student student = new Student();
        student.setName(request.getParameter("name"));
        student.setGender(request.getParameter("gender").equals("male"));
        student.setDob(Date.valueOf(request.getParameter("dob")));
        Department d = new Department();
        d.setId(Integer.parseInt(request.getParameter("did")));
        student.setDept(d);
        student.setCreateby(acc);
        StudentDBContext db = new StudentDBContext();
        db.insert(student);
        response.getWriter().println(student.getId() + " was inserted successful!");
    }

    @Override
    protected void doAuthGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        DepartmentDBContext db = new DepartmentDBContext();
        ArrayList<Department> depts = db.list();
        request.setAttribute("depts", depts);
        request.getRequestDispatcher("../view/student/create.jsp").forward(request, response);
    }

  

  

    

}
