/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.student;

import controller.authentication.BaseRequiredAuthorizationController;
import dao.StudentDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.Student;

/**
 *
 * @author sonnt
 */
public class DetailController extends BaseRequiredAuthorizationController {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        StudentDBContext db = new StudentDBContext();
        Student student = db.getStudentById(Integer.parseInt(request.getParameter("id")));
        request.setAttribute("student", student);
        request.getRequestDispatcher("../view/student/detail.jsp").forward(request, response);
    }
    
    @Override
    protected void doAuthPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        processRequest(request, response, acc);
    }

    @Override
    protected void doAuthGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        processRequest(request, response, acc);
    }
   
    
}
