/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.student;

import controller.authentication.BaseRequiredAuthorizationController;
import dao.DepartmentDBContext;
import dao.StudentDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.ArrayList;
import model.Account;
import model.Department;
import model.Student;

/**
 *
 * @author sonnt
 */
public class UpdateController extends BaseRequiredAuthorizationController {

    @Override
    protected void doAuthPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        Student student = new Student();
        student.setId(Integer.parseInt(request.getParameter("id")));
        student.setName(request.getParameter("name"));
        student.setGender(request.getParameter("gender").equals("male"));
        student.setDob(Date.valueOf(request.getParameter("dob")));
        Department d = new Department();
        d.setId(Integer.parseInt(request.getParameter("did")));
        student.setDept(d);
        student.setCreateby(acc);
        StudentDBContext db = new StudentDBContext();
        db.update(student);
        response.sendRedirect("detail?id="+student.getId());
    
    }

    @Override
    protected void doAuthGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        DepartmentDBContext db = new DepartmentDBContext();
        ArrayList<Department> depts = db.list();
        request.setAttribute("depts", depts);
        
        int id = Integer.parseInt(request.getParameter("id"));
        StudentDBContext studentDB = new StudentDBContext();
        Student student = studentDB.getStudentById(id);
        request.setAttribute("student", student);
        
        request.getRequestDispatcher("../view/student/update.jsp").forward(request, response);
    
    
    }
   
    
}
