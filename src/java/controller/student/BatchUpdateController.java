/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.student;

import controller.authentication.BaseRequiredAuthorizationController;
import dao.StudentDBContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.ArrayList;
import model.Account;
import model.Department;
import model.Student;

/**
 *
 * @author sonnt
 */
public class BatchUpdateController extends BaseRequiredAuthorizationController {

    @Override
    protected void doAuthPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
        String[] ids = request.getParameterValues("sid");
        ArrayList<Student> students = new ArrayList<>();
        
        for (String id : ids) {
            String name = request.getParameter("name"+ id);
            Date dob = Date.valueOf(request.getParameter("dob"+id));
            boolean gender = request.getParameter("gender"+id).equals("male");
            int did = Integer.parseInt(request.getParameter("did"+id));
            Student s = new Student();
            s.setId(Integer.parseInt(id));
            s.setName(name);
            s.setDob(dob);
            s.setGender(gender);
            Department d = new Department();
            d.setId(did);
            s.setDept(d);
            students.add(s);
        }
        StudentDBContext db = new StudentDBContext();
        db.updateBatch(students);
        response.sendRedirect("list");
    }

    @Override
    protected void doAuthGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException {
    }
   
}
