/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.authentication;

import dao.AccountDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import model.Account;
import model.Feature;

/**
 *
 * @author sonnt
 */
public abstract class BaseRequiredAuthorizationController
        extends BaseRequiredAuthenticaticationController
{
    private boolean isAuthorized(Account acc,HttpServletRequest request)
    {
        String accessedURL = request.getServletPath();
        AccountDBContext db = new AccountDBContext();
        ArrayList<Feature> features = db.getPermissionsBy(acc.getUsername());
        for (Feature feature : features) {
            if(feature.getUrl().equals(accessedURL))
                return true;
        }
        return false;
    }

    @Override
    protected  void doPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException
    {
        if(isAuthorized(acc, request))
        {
            //do business
            doAuthPost(request, response, acc);
        }
        else
        {
            response.getWriter().println("access denied");
        }
    }
protected abstract void doAuthPost(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException;
protected abstract void doAuthGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException;
    
    @Override
    protected  void doGet(HttpServletRequest request, HttpServletResponse response, Account acc) throws ServletException, IOException
    {
        if(isAuthorized(acc, request))
        {
            //do business
            doAuthGet(request, response, acc);
        }
        else
        {
            response.getWriter().println("access denied");
        }
    }
    
    
}
