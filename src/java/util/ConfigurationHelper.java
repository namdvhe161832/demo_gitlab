/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import jakarta.servlet.http.HttpServletRequest;

/**
 *
 * @author sonnt
 */
public class ConfigurationHelper {
    public static int getPageSize(HttpServletRequest request)
    {
      return Integer.parseInt(request.getServletContext().getInitParameter("pagesize"));
    }
    
}
