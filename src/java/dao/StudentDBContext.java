/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Department;
import model.Student;

/**
 *
 * @author sonnt
 */
public class StudentDBContext extends DBContext {

    public ArrayList<Student> list() {
        ArrayList<Student> students = new ArrayList<>();
        try {
            String sql = "SELECT [sid],sname,gender,dob,d.did,d.dname \n"
                    + "	FROM StudentTBL s INNER JOIN DepartmentTBL d\n"
                    + "		ON s.did = d.did\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Student s = new Student();
                s.setId(rs.getInt("sid"));
                s.setName(rs.getString("sname"));
                s.setGender(rs.getBoolean("gender"));
                s.setDob(rs.getDate("dob"));
                Department d = new Department();
                d.setId(rs.getInt("did"));
                d.setName(rs.getString("dname"));
                s.setDept(d);
                students.add(s);
            }

        } catch (SQLException ex) {
            Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return students;
    }

    public void updateBatch(ArrayList<Student> students) {
        try {
            connection.setAutoCommit(false);

            for (Student s : students) {
                String sql = "UPDATE [StudentTBL]\n"
                        + "   SET [sname] = ?\n"
                        + "      ,[gender] = ?\n"
                        + "      ,[dob] = ?\n"
                        + "      ,[did] = ?\n"
                        + " WHERE [sid] = ?";
                PreparedStatement stm = connection.prepareStatement(sql);
                stm.setString(1, s.getName());
                stm.setBoolean(2, s.isGender());
                stm.setDate(3, s.getDob());
                stm.setInt(4, s.getDept().getId());
                stm.setInt(5, s.getId());
                stm.executeUpdate();
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void insert(Student s) {
        try {
            //kiểm soát chính xác khi nào dữ liệu được commit
            connection.setAutoCommit(false);
            String sql = "INSERT INTO [StudentTBL]\n"
                    + "           ([sname]\n"
                    + "           ,[gender]\n"
                    + "           ,[dob]\n"
                    + "           ,[did]\n"
                    + "           ,[createdby])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, s.getName());
            stm.setBoolean(2, s.isGender());
            stm.setDate(3, s.getDob());
            stm.setInt(4, s.getDept().getId());
            stm.setString(5, s.getCreateby().getUsername());
            stm.executeUpdate();

            String sql_getsid = "SELECT @@IDENTITY as [sid]";
            PreparedStatement stm_getID = connection.prepareStatement(sql_getsid);
            ResultSet rs = stm_getID.executeQuery();
            if (rs.next()) {
                s.setId(rs.getInt("sid"));
            }
            // để xác nhận giao dịch
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Student getStudentById(int id) {
        try {
            String sql = "SELECT [sid],sname,gender,dob,d.did,d.dname \n"
                    + "	FROM StudentTBL s INNER JOIN DepartmentTBL d\n"
                    + "		ON s.did = d.did\n"
                    + "WHERE [sid] = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Student s = new Student();
                s.setId(rs.getInt("sid"));
                s.setName(rs.getString("sname"));
                s.setGender(rs.getBoolean("gender"));
                s.setDob(rs.getDate("dob"));
                Department d = new Department();
                d.setId(rs.getInt("did"));
                d.setName(rs.getString("dname"));
                s.setDept(d);
                return s;
            }

        } catch (SQLException ex) {
            Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void update(Student s) {
        try {
            String sql = "UPDATE [StudentTBL]\n"
                    + "   SET [sname] = ?\n"
                    + "      ,[gender] = ?\n"
                    + "      ,[dob] = ?\n"
                    + "      ,[did] = ?\n"
                    + " WHERE [sid] = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, s.getName());
            stm.setBoolean(2, s.isGender());
            stm.setDate(3, s.getDob());
            stm.setInt(4, s.getDept().getId());
            stm.setInt(5, s.getId());
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(StudentDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
