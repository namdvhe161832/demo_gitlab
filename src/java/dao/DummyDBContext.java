/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Department;
import model.Dummy;

/**
 *
 * @author sonnt
 */
public class DummyDBContext extends DBContext {

    public ArrayList<Dummy> list(int pageindex, int pagesize) {
        ArrayList<Dummy> dummies = new ArrayList<>();
        try {
            String sql = "SELECT id,name \n"
                    + " FROM Dummy\n"
                    + " ORDER BY id ASC \n"
                    + " OFFSET (?-1)*? ROWS     \n"
                    + " FETCH NEXT ? ROWS ONLY;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pageindex);
            stm.setInt(2, pagesize);
            stm.setInt(3, pagesize);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Dummy d = new Dummy();
                d.setId(rs.getInt("id"));
                d.setName(rs.getString("name"));
                dummies.add(d);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DummyDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dummies;
    }
    
    public int countForList() {
        try {
            String sql = "SELECT COUNT(*) as total FROM Dummy";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
               return rs.getInt("total");
            }

        } catch (SQLException ex) {
            Logger.getLogger(DummyDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
