package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Certificate;
;

public class CertificateDBContext extends DBContext{
    public ArrayList<Certificate> list() {
        
        ArrayList<Certificate> Certificates = new ArrayList<>();
        try {
            String sql = "SELECT cid,cname FROM CertificateTBL";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next())
            {
                Certificate d = new Certificate();
                d.setCid(rs.getInt("cid"));
                d.setCname(rs.getString("cname"));
                Certificates.add(d);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SkillDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Certificates;
        
    }
}
