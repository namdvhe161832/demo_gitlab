/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Department;
import model.Feature;
import model.Student;

/**
 *
 * @author sonnt
 */
public class AccountDBContext extends DBContext {

    public Account getAccountBy(String username, String password) {
        try {
            String sql = "SELECT username,displayname FROM AccountTBL\n"
                    + "WHERE username = ?\n"
                    + "AND password = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Account account = new Account();
                account.setUsername(username);
                account.setDisplayname(rs.getString("displayname"));
                return account;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Feature> getPermissionsBy(String authUser) {
        ArrayList<Feature> features = new ArrayList<>();
        try {
            String sql = "SELECT ra.username,r.rid, r.rname,f.fid,f.url,f.fname FROM RoleAccountTBL ra \n"
                    + "			  LEFT JOIN RoleTBL r ON r.rid = ra.rid\n"
                    + "			  LEFT JOIN RoleFeatureTBL rf ON r.rid = rf.rid\n"
                    + "			  LEFT JOIN FeatureTBL f ON f.fid = rf.fid\n"
                    + "WHERE ra.username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, authUser);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                int fid = rs.getInt("fid");
                if (fid != 0) {
                    Feature f = new Feature();
                    f.setId(fid);
                    f.setUrl(rs.getString("url"));
                    features.add(f);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return features;
    }

    
}
