/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Certificate {
    private int cid;
    private String cname;
    private ArrayList<Student> students = new ArrayList<>();

    public int getCid() {
        return cid;
    }

    public void setCid(int id) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String name) {
        this.cname = cname;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    
}
}
