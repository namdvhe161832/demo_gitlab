package model;


import java.sql.Date;
import java.util.ArrayList;
import model.Account;
import model.Department;
import model.Skill;

public class Student {
    private int id;
    private String name;
    private boolean gender;
    private Department dept;
    private Date dob;
    private Account createby;
    private ArrayList<Skill> skills = new ArrayList<>();

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public Department getDept() {
        return dept;
    }

    public void setDept(Department dept) {
        this.dept = dept;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Account getCreateby() {
        return createby;
    }

    public void setCreateby(Account createby) {
        this.createby = createby;
    }
    
}