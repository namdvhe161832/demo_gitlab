
USE [BLOCK5SUMMER2023]
GO
/****** Object:  Table [dbo].[AccountTBL]    Script Date: 8/10/2023 12:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountTBL](
	[username] [varchar](150) NOT NULL,
	[password] [varchar](150) NOT NULL,
	[displayname] [varchar](150) NULL,
 CONSTRAINT [PK_AccountTBL] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DepartmentTBL]    Script Date: 8/10/2023 12:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepartmentTBL](
	[did] [int] NOT NULL,
	[dname] [varchar](150) NOT NULL,
 CONSTRAINT [PK_DepartmentTBL] PRIMARY KEY CLUSTERED 
(
	[did] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeatureTBL]    Script Date: 8/10/2023 12:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeatureTBL](
	[fid] [int] NOT NULL,
	[url] [varchar](max) NOT NULL,
	[fname] [varchar](150) NOT NULL,
 CONSTRAINT [PK_FeatureTBL] PRIMARY KEY CLUSTERED 
(
	[fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleAccountTBL]    Script Date: 8/10/2023 12:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleAccountTBL](
	[rid] [int] NOT NULL,
	[username] [varchar](150) NOT NULL,
 CONSTRAINT [PK_RoleAccountTBL] PRIMARY KEY CLUSTERED 
(
	[rid] ASC,
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleFeatureTBL]    Script Date: 8/10/2023 12:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleFeatureTBL](
	[rid] [int] NOT NULL,
	[fid] [int] NOT NULL,
 CONSTRAINT [PK_RoleFeatureTBL] PRIMARY KEY CLUSTERED 
(
	[rid] ASC,
	[fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleTBL]    Script Date: 8/10/2023 12:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleTBL](
	[rid] [int] NOT NULL,
	[rname] [varchar](150) NOT NULL,
 CONSTRAINT [PK_RoleTBL] PRIMARY KEY CLUSTERED 
(
	[rid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentTBL]    Script Date: 8/10/2023 12:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentTBL](
	[sid] [int] IDENTITY(1,1) NOT NULL,
	[sname] [varchar](150) NOT NULL,
	[gender] [bit] NOT NULL,
	[dob] [date] NOT NULL,
	[did] [int] NOT NULL,
	[createdby] [varchar](150) NOT NULL,
 CONSTRAINT [PK_StudentTBL] PRIMARY KEY CLUSTERED 
(
	[sid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleAccountTBL]  WITH CHECK ADD  CONSTRAINT [FK_RoleAccountTBL_AccountTBL] FOREIGN KEY([username])
REFERENCES [dbo].[AccountTBL] ([username])
GO
ALTER TABLE [dbo].[RoleAccountTBL] CHECK CONSTRAINT [FK_RoleAccountTBL_AccountTBL]
GO
ALTER TABLE [dbo].[RoleAccountTBL]  WITH CHECK ADD  CONSTRAINT [FK_RoleAccountTBL_RoleTBL] FOREIGN KEY([rid])
REFERENCES [dbo].[RoleTBL] ([rid])
GO
ALTER TABLE [dbo].[RoleAccountTBL] CHECK CONSTRAINT [FK_RoleAccountTBL_RoleTBL]
GO
ALTER TABLE [dbo].[RoleFeatureTBL]  WITH CHECK ADD  CONSTRAINT [FK_RoleFeatureTBL_FeatureTBL] FOREIGN KEY([fid])
REFERENCES [dbo].[FeatureTBL] ([fid])
GO
ALTER TABLE [dbo].[RoleFeatureTBL] CHECK CONSTRAINT [FK_RoleFeatureTBL_FeatureTBL]
GO
ALTER TABLE [dbo].[RoleFeatureTBL]  WITH CHECK ADD  CONSTRAINT [FK_RoleFeatureTBL_RoleTBL] FOREIGN KEY([rid])
REFERENCES [dbo].[RoleTBL] ([rid])
GO
ALTER TABLE [dbo].[RoleFeatureTBL] CHECK CONSTRAINT [FK_RoleFeatureTBL_RoleTBL]
GO
ALTER TABLE [dbo].[StudentTBL]  WITH CHECK ADD  CONSTRAINT [FK_StudentTBL_AccountTBL] FOREIGN KEY([createdby])
REFERENCES [dbo].[AccountTBL] ([username])
GO
ALTER TABLE [dbo].[StudentTBL] CHECK CONSTRAINT [FK_StudentTBL_AccountTBL]
GO
ALTER TABLE [dbo].[StudentTBL]  WITH CHECK ADD  CONSTRAINT [FK_StudentTBL_DepartmentTBL] FOREIGN KEY([did])
REFERENCES [dbo].[DepartmentTBL] ([did])
GO
ALTER TABLE [dbo].[StudentTBL] CHECK CONSTRAINT [FK_StudentTBL_DepartmentTBL]
GO


--Insert account
INSERT INTO [dbo].[AccountTBL]
           ([username]
           ,[password]
           ,[displayname])
     VALUES
           ('admin','12345678','@displayname00001')
		   ,('staff','12345678','@displayname00002')
		   ,('teacher','12345678','@displayname00003')
		   ,('parent','12345678','@displayname00004')
		   ,('student','12345678','@displayname00005')
--Insert department
INSERT INTO [dbo].[DepartmentTBL]
           ([did]
           ,[dname])
     VALUES(1, 'Information Assurance')
	 ,(2,'Computer Science')
	 ,(3,'Software Engineering')
	 ,(4,'Security Information')
	 ,(5,'Embedded Software Engineering')

-- Insert RoleTBL
INSERT INTO [dbo].[RoleTBL]
           ([rid],[rname])
		   VALUES(1,'Admin')
		   ,(2,'Staff')
		   ,(3,'Teacher')
		   ,(4,'Parent')
		   ,(5,'Student')

-- Insert FeatureTBL
INSERT INTO [dbo].[FeatureTBL]
			([fid],[url],[fname]) VALUES (1,'/student/list','View a list student')
										,(2,'/student/create','New a student')
										,(3,'/student/update','Update a specific student')
										,(4,'/student/remove','Change status student')
										,(5,'/student/detail','View a specific student')
										,(6,'/student/updatebatch','Batch update list student')


-- Insert RoleAccountTBL
INSERT INTO [dbo].[RoleAccountTBL]
			([rid],[username]) VALUES (1,'admin')
									  ,(2,'staff')
									  ,(3,'teacher')
									  ,(4,'parent')
									  ,(5,'student')
-- Insert RoleFeatureTBL
INSERT INTO [dbo].[RoleFeatureTBL]
			([rid],[fid]) VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(2,1),(2,3),(2,5),(2,6),(3,1),(3,5),(5,3),(5,5),(5,6)





CREATE TABLE [dbo].[Dummy](
  [id] int NOT NULL PRIMARY KEY,
  [name] varchar(10) NOT NULL
)


DECLARE @COUNT INT = 1
WHILE @COUNT <= 2000
BEGIN 
 INSERT INTO Dummy(id,name) VALUES (@COUNT,'MR ' + CAST(@COUNT AS VARCHAR(10)));
 SET @COUNT = @COUNT + 1
END