<%-- 
    Document   : list
    Created on : Aug 17, 2023, 10:17:44 AM
    Author     : sonnt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dummy Object Table</title>
    <script src="../js/pagger.js" type="text/javascript"></script>
    <link href="../css/pagger.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <h1>Dummy Object Table</h1>
    <div id ="toppagger" class="pagger"></div>
    <table border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="dummy" items="${requestScope.dummies}">
                <tr>
                    <td>${dummy.id}</td>
                    <td>${dummy.name}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <div id ="botpagger" class="pagger"></div>
    <script>
        generatePagger("toppagger",${requestScope.pageindex}
                                  ,${requestScope.totalpage},2,"list");
        generatePagger("botpagger",${requestScope.pageindex}
                                  ,${requestScope.totalpage},2,"list");                          
    </script>
</body>
</html>
