<%-- 
    Document   : create
    Created on : Aug 14, 2023, 10:11:13 AM
    Author     : sonnt
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Student Insert Form</title>
</head>
<body>

<h2>Insert Student Information</h2>

<form action="create" method="post">
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" required><br><br>

    <label>Gender:</label>
    <input type="radio" id="male" checked="checked" name="gender" value="Male" required>
    <label for="male">Male</label>
    <input type="radio" id="female" name="gender" value="Female" required>
    <label for="female">Female</label><br><br>

    <label for="dob">Date of Birth:</label>
    <input type="date" id="dob" name="dob" required><br><br>

    <label for="department">Department:</label>
    <select id="did" name="did" required>
        <c:forEach items="${requestScope.depts}" var="d">
            <option value="${d.id}">${d.name}</option>
        </c:forEach>
    </select><br><br>

    <input type="submit" value="Save">
</form>

</body>
</html>

