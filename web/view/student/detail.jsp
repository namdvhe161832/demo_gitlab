<%-- 
    Document   : detail
    Created on : Aug 14, 2023, 10:11:21 AM
    Author     : sonnt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Student Information</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 0;
        }

        h1 {
            background-color: #4285f4;
            color: white;
            padding: 20px;
            margin: 0;
        }

        .student-info {
            background-color: white;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            margin: 20px;
            padding: 20px;
        }

        .student-info p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <h1>Student Information</h1>
    <div class="student-info">
        <c:choose>
            <c:when test="${requestScope.student ne null}">
                <p><strong>ID:</strong> ${requestScope.student.id}</p>
                <p><strong>Name:</strong> ${requestScope.student.name}</p>
                <p><strong>Gender:</strong> ${requestScope.student.gender ? 'Male' : 'Female'}</p>
                <p><strong>Date of Birth:</strong> ${requestScope.student.dob}</p>
                <p><strong>Department:</strong> ${requestScope.student.dept.name}</p>
            </c:when>
            <c:otherwise>
                <p>No student information available.</p>
            </c:otherwise>
        </c:choose>
    </div>
</body>
</html>

