<%-- 
    Document   : list
    Created on : Aug 14, 2023, 10:11:34 AM
    Author     : sonnt
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List student</title>
    </head>
    <style>
        .body {
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        a{
            text-decoration: none;
            color: black;
        }

        .header {
            height: 3rem;
            box-shadow: 0 0.25rem 0.25rem 0 rgb(0 0 0 / 25%);
            background-color: #fff;
        }

        .header h3 {
            font-size: 1.25rem;
            font-style: normal;
            line-height: 3rem;
            margin-left: 2%;
        }

        .upper-table{
            margin-bottom: 2%;
        }
        .left-upper {
            display: inline-block;
            margin-left: 2%;
        }

        .right-upper {
            float: right;
            line-height: 3.1875rem;
            margin-right: 4%;
        }

        form {
            display: inline-block;           
        }
        form input[type='search'] {
            padding: 4% 0;
        }

        button {
            background-color: #42BBFF;
            border: 0;
            padding: 0.625ren;
            border-radius: 0.25rem;
        }

        .table {
            width: 80%;
            margin: auto;
        }

        #table-std {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #table-std td,
        #table-std th {
            border: 1px solid #ddd;
            padding: 0.5rem;
        }

        #table-std tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #table-std tr:hover {
            background-color: #ddd;
        }

        #table-std th {
            padding-top: 0.75rem;
            padding-bottom: 0.75rem;
            text-align: left;
            background-color: #f9f9f9;
        }
    </style>
    <script>
        function showEditMode(id)
        {
            showEditModeForName(id);
            showEditModeForDob(id);
            showEditModeForDid(id);
            showEditModeForGender(id);

            var btn_Save = document.getElementById("btn_Save" + id);
            btn_Save.style.display = "block";

            var btn_Edit = document.getElementById("btn_Edit" + id);
            btn_Edit.style.display = "none";
        }
        function showViewMode(id)
        {
            showViewModeForName(id);
            showViewModeForDob(id);
            showViewModeForDid(id);
            showViewModeForGender(id);
            var btn_Save = document.getElementById("btn_Save" + id);
            btn_Save.style.display = "none";
            var btn_Edit = document.getElementById("btn_Edit" + id);
            btn_Edit.style.display = "block";
        }

        //control for Name
        function showEditModeForName(id)
        {
            var sp_view = document.getElementById("sp_name_view" + id);
            sp_view.style.display = "none";

            var txt_edit = document.getElementById("txt_name_edit" + id);
            txt_edit.style.display = "block";
        }

        function showViewModeForName(id)
        {
            var sp_view = document.getElementById("sp_name_view" + id);
            sp_view.style.display = "block";
            var txt_edit = document.getElementById("txt_name_edit" + id);
            txt_edit.style.display = "none";
            sp_view.innerHTML = txt_edit.value;
        }

        //control for Dob 
        function showEditModeForDob(id)
        {
            var sp_view = document.getElementById("sp_dob_view" + id);
            sp_view.style.display = "none";

            var txt_edit = document.getElementById("txt_dob_edit" + id);
            txt_edit.style.display = "block";
        }

        function showViewModeForDob(id)
        {
            var sp_view = document.getElementById("sp_dob_view" + id);
            sp_view.style.display = "block";
            var txt_edit = document.getElementById("txt_dob_edit" + id);
            txt_edit.style.display = "none";
            sp_view.innerHTML = txt_edit.value;
        }

        //control for Department
        function showEditModeForDid(id)
        {
            var sp_view = document.getElementById("sp_did_view" + id);
            sp_view.style.display = "none";

            var txt_edit = document.getElementById("txt_did_edit" + id);
            txt_edit.style.display = "block";
        }

        function showViewModeForDid(id)
        {
            var sp_view = document.getElementById("sp_did_view" + id);
            sp_view.style.display = "block";
            var txt_edit = document.getElementById("txt_did_edit" + id);
            txt_edit.style.display = "none";
            sp_view.innerHTML = txt_edit.options[txt_edit.selectedIndex].innerHTML;
        }

        //control for Gender
        function showEditModeForGender(id)
        {
            var sp_view = document.getElementById("sp_gender_view" + id);
            sp_view.style.display = "none";

            var txt_edit_Male = document.getElementById("txt_genderMale_edit" + id);
            txt_edit_Male.style.display = "block";
            var txt_edit_Female = document.getElementById("txt_genderFemale_edit" + id);
            txt_edit_Female.style.display = "block";
            var lbl_edit_Male = document.getElementById("lbl_genderMale_edit" + id);
            lbl_edit_Male.style.display = "block";
            var lbl_edit_Female = document.getElementById("lbl_genderFemale_edit" + id);
            lbl_edit_Female.style.display = "block";
        }

        function showViewModeForGender(id)
        {
            var sp_view = document.getElementById("sp_gender_view" + id);
            sp_view.style.display = "block";

            var txt_edit_Male = document.getElementById("txt_genderMale_edit" + id);
            txt_edit_Male.style.display = "none";
            var txt_edit_Female = document.getElementById("txt_genderFemale_edit" + id);
            txt_edit_Female.style.display = "none";
            var lbl_edit_Male = document.getElementById("lbl_genderMale_edit" + id);
            lbl_edit_Male.style.display = "none";
            var lbl_edit_Female = document.getElementById("lbl_genderFemale_edit" + id);
            lbl_edit_Female.style.display = "none";

            sp_view.innerHTML = txt_edit_Male.checked ? "Male" : "Female";
        }

    </script>
    <body>
        <div class="header">
            <h3 class="title-url">List student</h3>
        </div>

        <!--  And search -->
        <div class="upper-table">
            <div class="left-upper">
                <p><i><b>${requestScope.sizeStudent}</b></i> of students were found. </p>
            </div>

            <div class="right-upper">
                <form>
                    <input type="search" name="" id="" placeholder="Search">
                    <button>GO</button>
                </form>
                <button><a href="create">New Student</a></button>
            </div>
        </div>
    </div>

    <!--news--> 
    <div class="table">
        <form action="updatebatch" method="post" style="width: 100%;">
            <table id="table-std">
                <tr>
                    <th>Numerical order</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Dob</th>
                    <th>Department</th>
                    <th>Infor</th>
                    <th>Command</th>
                </tr>
                <c:forEach items="${requestScope.listStudent}" var="s" varStatus="i">
                    <tr>
                        <td>
                            ${i.index + 1}
                            <input type="hidden" name="sid" value="${s.id}" />
                        </td>
                        <td>
                            <span id="sp_name_view${s.id}">${s.name}</span>
                            <input id="txt_name_edit${s.id}" style="display:none" type="text" name="name${s.id}" 
                                   value="${s.name}"/>
                        </td>
                        <td>
                            <span id="sp_gender_view${s.id}">${s.gender?"Male":"Female"}</span>
                            <input id="txt_genderMale_edit${s.id}" style="display:none" type="radio" value="male" name="gender${s.id}" 
                                   <c:if test="${s.gender}">
                                       checked="checked"
                                   </c:if>
                                   /> <span id="lbl_genderMale_edit${s.id}" style="display:none">Male</span>
                            <input id="txt_genderFemale_edit${s.id}" style="display:none" type="radio" value="female" name="gender${s.id}" 
                                   <c:if test="${!s.gender}">
                                       checked="checked"
                                   </c:if>
                                   /> <span style="display:none" id="lbl_genderFemale_edit${s.id}">Female</span>
                        </td>
                        <td>
                            <span id="sp_dob_view${s.id}">${s.dob}</span>
                            <input id="txt_dob_edit${s.id}" style="display:none" type="date" name="dob${s.id}" 
                                   value="${s.dob}"/>
                        </td>
                        <td>
                            <span id="sp_did_view${s.id}">${s.dept.name}</span>
                            <select id="txt_did_edit${s.id}" style="display:none"
                                    name="did${s.id}"
                                    >
                                <c:forEach items="${requestScope.depts}" var="d">
                                    <option value="${d.id}"
                                            <c:if test="${s.dept.id eq d.id}">
                                                selected="selected"
                                            </c:if>
                                            >${d.name}</option>
                                </c:forEach>
                            </select>
                        </td>
                         <td><a href="detail?id=${s.id}"><b>i</b></a></td>
                        <td>
                            <input type="button" value="EDIT" id="btn_Edit${s.id}" onclick="showEditMode(${s.id});" />
                            <input style="display:none" type="button" id="btn_Save${s.id}" value="SAVE" onclick="showViewMode(${s.id})"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <input type="submit" value="Save All"/>
        </form>
    </div>
</body>
</html>
