/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

function generatePagger(id,pageindex,totalpage,gap,action)
{
    var content = "";
    
    //first 
    if(pageindex - gap>1)
        content += "<a href=\""+action+"?page=1\">First</a>";
    
    //before 
    for (var i = pageindex - gap; i < pageindex; i++) {
        if(i > 0)
            content += "<a href=\""+action+"?page="+i+"\">"+i+"</a>";
    }
    
    //page index
    content += "<span>"+pageindex+"</span>";  
    
    //after 
    for (var i = pageindex + 1; i <= pageindex+gap; i++) {
        if(i < totalpage)
            content += "<a href=\""+action+"?page="+i+"\">"+i+"</a>";
    }
    
    //last 
    if(pageindex + gap < totalpage)
        content += "<a href=\""+action+"?page="+totalpage+"\">Last</a>";
    
    
    
    var container = document.getElementById(id);
    container.innerHTML = content;
    
}
